=begin
This  class implement a 'RangeList'
=end

class RangeList
  attr_reader :range_list
  def initialize
    @range_list = []
  end

  def add(range = [])
    # implement add
    return range_list unless validate(range)
    temp_range = (range[0]..range[1]-1).to_a
    @range_list += temp_range
    @range_list = @range_list.uniq.sort
  end

  def remove(range = [])
    # implement remove
    return range_list unless  validate(range)
    temp_range = (range[0]..range[1]-1).to_a
    @range_list -= temp_range
    @range_list = @range_list.sort
  end

  def print
    # implement print
    final_result = ''
    temp_range = []
    sub_range = []
    if @range_list.empty?
      return final_result
    end
    @range_list.each do |item|
      if sub_range.empty? || (item -1 == sub_range[-1])
        sub_range << item
      else
        temp_range << sub_range
        sub_range = [item]
      end
    end

    temp_range << sub_range unless sub_range.empty?

    temp_range.each do |slice|
      final_result += "[#{slice[0]}, #{slice[-1] + 1})"
      final_result += ' ' if slice !=  temp_range[-1]
    end

    puts final_result
  end

  def validate(range)
    return false unless range.is_a?(Array)
    return false unless range.size == 2
    return false unless range[0] < range[1]
    true
  end
  private :validate

end